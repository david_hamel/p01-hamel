//
//  ViewController.h
//  p01-hamel
//
//  Created by David Hamel on 1/22/17.
//  Copyright © 2017 dhamel1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    int counter;
}
@property (weak, nonatomic) IBOutlet UIButton *mainButon;

@property (weak, nonatomic) IBOutlet UILabel *outputLabel;

- (IBAction)changeMessage:(id)sender;

@end

