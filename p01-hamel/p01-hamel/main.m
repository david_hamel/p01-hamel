//
//  main.m
//  p01-hamel
//
//  Created by David Hamel on 1/22/17.
//  Copyright © 2017 dhamel1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
