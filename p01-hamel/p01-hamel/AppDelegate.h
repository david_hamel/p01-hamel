//
//  AppDelegate.h
//  p01-hamel
//
//  Created by David Hamel on 1/22/17.
//  Copyright © 2017 dhamel1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

