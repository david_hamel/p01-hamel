//
//  ViewController.m
//  p01-hamel
//
//  Created by David Hamel on 1/22/17.
//  Copyright © 2017 dhamel1. All rights reserved.
//

#import "ViewController.h"
#import "stdlib.h"

@interface ViewController ()

@end

@implementation ViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)changeMessage:(id)sender {
    counter++;
    float randX,randY;
    
    //I learned about arc4random_uniform() from this SO thread: http://stackoverflow.com/questions/160890/generating-random-numbers-in-objective-c
    //does not check the orientation of the screen
    randX = arc4random_uniform(400);
    randY = arc4random_uniform(600);
    
    //NSLog(@"X value: %f",randX);
    //NSLog(@"X value: %f",randY);
    //NSLog(@"Button pressed");
    
    NSString *labelText = [NSString stringWithFormat:@"Score: %d",counter];
    [_outputLabel setText:labelText];
    _mainButon.center = CGPointMake(randX,randY);
}
@end
